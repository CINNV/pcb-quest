@gusj, le 17/01/2019


Travail attendu:
----------------


Obj: documenter le procéder de prototypage d'un circuit imprimé



1. Créer projet Gitlab
*  créer + prise en main
*  mettre le plan
*  choisir un circuit à réaliser qui sera suivi en tuto

=> proposition de rédaction
==> dès la première pièce, prendre des notes avec impressions écrans sur un Word ou Gitlab (brouillon)
==> après la première pièce réalisée, refaire avec un tuto mieux documenté en évitant erreurs
==> refaire faire par d'autres étudiants

2. Eagle
*  [[[[ Sébastien Hamonic peut être de bons conseils sur ce logiciel ]]]]
*  2.1) installation -> lien tuto sur gitlab
*  2.2) dessiner schéma
	-> chercher tuto (cf. Moodle ou internet)
	-> rédiger tuto des points importants
*  2.3) dessin PCB
	-> idem recherche tuto
	-> router en double face
	-> rédiger tuto avec impressions écrans
*  2.4) Export
	-> voir avec moi
	-> tuto avec impressions écrans

3. Fraiseuse CIF
*  3.1) doc
	-> mettre la doc existante numérique/numérisée sur Gitlab
	-> rédiger présentation du matos, outils et suite logiciel sur Gitlab
*  3.1) import perceval
*  3.2) générer trajectoires pour une face
	-> trajectoires trous, pistes, détourages, hachurages...
	-> vérif outils
*  3.4) préparer plaque cuivre-époxy
	-> découpe brute aux dimensions
	-> attention bord de référence pour 
*  3.3) usinage une face
	-> tuto avec photos/vidéos : de la mise en place à l'usinage
	-> changement outil
	-> sécurité !!!
	-> démarrage machine
	-> bridage pièce
	-> référence
	-> ...
*  3.4) usinage 2ème face
	-> retouor sur perceval, logiciel...
	-> positionnement et centrage
	-> usinage
*  3.5) ranger/nettoyer

4. Gravure chimique
----> cf. Séb.

5. Vérif
6. Soudure
*  6.1) poste de travail à mettre en place
*  6.2) protocole
7. test circuit

