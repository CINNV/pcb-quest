# PCB-Quest

Intitulé du projet didactique de 2ème année Mécatronique à l'ENS Rennes :

**Didactisation de la démarche de conception électronique : du modèle numérique au circuit imprimé réalisé.**


<br>


[Premier tutoriel : Téléchargement et Installation d'EAGLE](https://gitlab.com/CINNV/pcb-quest/wikis/Premier-tutoriel-:-T%C3%A9l%C3%A9chargement-et-Installation-d'EAGLE)

[Second Tutoriel : Création du modèle numérique d'un circuit imprimé de clignoteur](https://gitlab.com/CINNV/pcb-quest/wikis/Second-Tutoriel-:-Cr%C3%A9ation-du-mod%C3%A8le-num%C3%A9rique-d'un-circuit-imprim%C3%A9-de-clignoteur)

[Troisième tutoriel : Téléchargement et Installation de la suite logiciels Galaad](https://gitlab.com/CINNV/pcb-quest/wikis/Troisi%C3%A8me-tutoriel-:-T%C3%A9l%C3%A9chargement-et-Installation-de-la-suite-logiciels-Galaad)

[Quatrième tutoriel : Export EAGLE & Import Galaad du modèle de circuit imprimé](https://gitlab.com/CINNV/pcb-quest/wikis/Quatrième-tutoriel-:-Export-EAGLE-&-Import-Galaad-du-modèle-de-circuit-imprimé)

[Cinquième tutoriel : Préparation numérique et Simulation](https://gitlab.com/CINNV/pcb-quest/wikis/Cinqui%C3%A8me-tutoriel-:-Pr%C3%A9paration-num%C3%A9rique-et-Simulation)

[Sixième tutoriel : Préparation et opération d'usinage & perçage](https://gitlab.com/CINNV/pcb-quest/wikis/Sixi%C3%A8me-tutoriel-:-Pr%C3%A9paration-et-op%C3%A9ration-d'usinage-&-per%C3%A7age)

[Vidéo de l'usinage et résultat](https://gitlab.com/CINNV/pcb-quest/wikis/Vid%C3%A9o-de-l'usinage-et-r%C3%A9sultat)

<br>


<h2>Liste des contributeurs</h2>

- Gurvan Jodin - @gusj - Post-Doctorant ENS Rennes SATIE

- Nicolas Vincent - @CINNV - M1 ISC ENS Rennes


<br>


<h2>TODO et Pistes d'améliorations</h2>

- Revoir et améliorer la mise en forme markdown
- Améliorer l'archivage/arborescence et classer par établissements
- Réviser le découpage des tutoriels en ajoutant davantage d'ancres et de sous-parties